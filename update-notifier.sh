#!/usr/bin/env sh

DEFAULTS_FILE="/etc/default/update-notifier"

. "${DEFAULTS_FILE}"

if test "${DEBUG}" = "1" ; then
	set -x
	error_log="${HOME}/update-notifier.log"
	echo "$(date)" > "$error_log"
	exec 2>>"$error_log"
fi


config () {

	options='none!terminal!gui'
	final_options="$( echo "${options}" | tr '!' '\n' | awk "BEGIN{a=0} a==0 && /${FRONTEND}/{a=1;print \"^\"\$0;} "'!'"/${FRONTEND}/{print} END{if(a==0)print \"^${FRONTEND}\";}" | tr '\n' '!' | sed -r -e 's/!$//;' )"

    results="$( yad --title="Settings" \
        --form --borders=5 \
        --field="Current settings\:
    Frontend\: ${FRONTEND}
    Interval\: ${INTERVAL}

Options for Front end\:
  none		Notify only.
  terminal	Notify and upgrade in root terminal.
  gui 		Notify and upgrade in Synaptic Package manager.

Interval is time to wait between notifications.
Examples: 8h for every 8 hours, 3d for every 3 days.:LBL" '' \
        --field="Front end:CB" "${final_options}" \
        --field="Interval" "${INTERVAL}" \
        --field="Additional settings are in ${DEFAULTS_FILE}.:LBL" '' )"
    ans="$?"

    results="$( echo "${results}" | sed -r -e "s:\|+:|:g" -e 's:^\|::' -e 's:\|$::' )"
    frontend="$( echo "${results}" | awk -F'|' '{print $1}' )"
    interval="$( echo "${results}" | awk -F'|' '{print $2}' )"

	if test "${ans}" = "1" ; then
        echo "Canceled."
		exit 0
    elif test "${FRONTEND}|${INTERVAL}" != "${frontend}|${interval}" ; then
        sed -i \
            -e "s/^.*FRONTEND=.*/FRONTEND=\"$frontend\"/" \
            -e "s/^.*INTERVAL=.*/INTERVAL=\"$interval\"/" "${DEFAULTS_FILE}"
        grep -qE "FRONTEND=" "${DEFAULTS_FILE}" 2>/dev/null || echo "FRONTEND=${frontend}" >> "${DEFAULTS_FILE}"
        grep -qE "INTERVAL=" "${DEFAULTS_FILE}" 2>/dev/null || echo "INTERVAL=${interval}" >> "${DEFAULTS_FILE}"
    else
        echo "No changes from current config."
    fi

}

edit_configs () {
	exit 0
}

while echo "${1}" | grep -q -E -e "-.*" 2>/dev/null ; do
	option="$1"
	case "$option" in

		-c|--config)
			config
			exit 0 ;;

		*) echo $"
	invalid option: $option \n\n
	Try:  $programname -h for full help. \n\n"
			exit 1 ;;
    esac
done

list=$(apt-get -s dist-upgrade | awk '/^Inst/ { print $2 }')

count=$(echo "$list" | wc -l)



if test -n "$list" ; then

	notify-send --expire-time=20000 --icon="/usr/share/pixmaps/updates.svg" \
"Available updates: $count

$list"

	yad --notification \
		--image="/usr/share/pixmaps/updates.svg" \
		--icon-size=22 \
		--command="/usr/bin/upgrade.sh" \
		--text="Available updates: $count
	_____________________

$list" &

fi

exit 0
