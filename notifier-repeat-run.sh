#!/usr/bin/env bash

source /etc/default/update-notifier

# Do not allow zero or null time interval.
if [[ "$INTERVAL" = [1-9]* ]] ; then
	sleep "$INTERVAL" && /usr/bin/update-notifier.sh; while sleep "$INTERVAL" ; do /usr/bin/update-notifier.sh; done
else
	yad --notification \
		--image="/usr/share/pixmaps/updates.svg" \
		--icon-size=22 \
		--text="
        Invalid time interval.
	_____________________

Check Update Notifier Settings  " &

	exit 0
fi
